import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUserData = {login: '', password: ''}
  constructor(private _auth: AuthService,
              private _router: Router,
              private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  loginUser(){
     
    this._auth.loginUser(this.loginUserData)
    .subscribe(
      res => {
        localStorage.setItem('token', res.token);
        this._auth.getUserInfo().subscribe(
          res =>{
            console.log(res);
            localStorage.setItem("id", '' + res.id);
            localStorage.setItem("funds", '' + res.funds);
            localStorage.setItem("name", res.name);
            localStorage.setItem("role", res.role);
            this._router.navigate(['events']);
          },
          err =>{console.log(err)
            this.showMsg(err.error)}
        )
      },
      err => {localStorage.setItem('token', '');
        this.showMsg(err.error);
      
    }
    )
  }

  showMsg(msg: string) {
    this._snackBar.open(msg, "", {
      duration: 4000,
    });
  }

}
