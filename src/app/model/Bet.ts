export class Bet {
    betResult: String;
    betValue: number;
    matchId: number;
    userId: number;
}