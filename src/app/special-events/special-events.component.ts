import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-special-events',
  templateUrl: './special-events.component.html',
  styleUrls: ['./special-events.component.css']
})
export class SpecialEventsComponent implements OnInit {
  money: MoneyTransfer;
  constructor(private _auth: AuthService, private _snackBar: MatSnackBar, private _router: Router) { 
    this.money = new MoneyTransfer();
    this.money.userId = Number(localStorage.getItem('id'));
    this.money.creditCardNumber = "";
    this.money.value = 0;
  }

  ngOnInit(): void {
  }

  payIn(){
    this._auth.payInMoney(this.money).subscribe(
      res => {
        this.showMsg("Wpłacono pieniądze");
        this._router.navigate(['events']);
      },
      err => {
        this.showMsg(err.error);
        console.log(err);
      }
    )
  }

  showMsg(msg: string) {
    this._snackBar.open(msg, "", {
      duration: 4000,
    });
  }

}

export class MoneyTransfer {
  creditCardNumber: string;
  userId: number;
  value: number;
}