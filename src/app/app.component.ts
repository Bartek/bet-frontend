import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  title = 'ZakladyBukmacherskie';
  user_funds = 0;
  user_name = "";

  constructor(private _authService: AuthService, private router: Router) {
    router.events.subscribe((e) => {
      if(!(e as any).shouldActivate){
        return;
      }

      var t = localStorage.getItem('token');
      if(t != null && t.length > 0){
        this._authService.getUserInfo().subscribe(
          res =>{
            localStorage.setItem("id", '' + res.id);
            localStorage.setItem("funds", '' + res.funds);
            localStorage.setItem("name", res.name);
            this.user_funds = res.funds;
            this.user_name = res.name;
            console.log("nnn  ");
          },
          err =>{console.log(err)}
        )
      }else {

      }

    })
  }

  userIsLogged(): Boolean{
    let role = localStorage.getItem('role');
    if(role != null && role.length >0){
      return true;
    }
    return false;
  }

  logoutUser():void{
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    localStorage.removeItem("name");
    localStorage.removeItem("token");
    this.router.navigate(['events']);
  }
}
