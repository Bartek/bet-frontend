import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerUserData = {email: '', password: '', login:''}
  constructor(private _auth: AuthService,
              private _snackBar: MatSnackBar,
              private _router: Router) { }

  ngOnInit() {
  }

  registerUser(){

    this._auth.registerUser(this.registerUserData)
    .subscribe(
      res => this.showMsg("Rejestracja przebiegła pomyślnie"),
      err => this.showMsg(err.error)
    )
  }

  showMsg(msg: string){
    this._snackBar.open(msg, "", {
      duration: 4000,
    });
  }
}
