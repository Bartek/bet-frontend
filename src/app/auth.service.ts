import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router'
import { Match } from './match';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { Bet } from './model/Bet';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _registerUrl = "http://localhost:8081/user/create";
  private _loginUrl = "http://localhost:8081/user/login";

  constructor(private http: HttpClient,
              private _router: Router) { }

  registerUser(user) {
    return this.http.post<any>(this._registerUrl, user)
  }

  loginUser(user) {
    return this.http.post<any>(this._loginUrl, user)
  }

  getHeaders(){
    return {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
  }

  getUserInfo() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };

    return this.http.get<user>('http://localhost:8081/user', this.getHeaders());
  }


  getMatches(): Observable<Match[]> {
    return this.http.get<Match[]>("http://localhost:8081/bet-app/match/active");
  }

  getEndMatches(): Observable<Match[]> {
    return this.http.get<Match[]>("http://localhost:8081/bet-app/match/ended-no-result");
  }

  createBet(bet){
    return this.http.post<any>("http://localhost:8081/bet-app/bet/create", bet);
  }

  createMatch(match){
    return this.http.post<any>("http://localhost:8081/bet-app/match/create", match, this.getHeaders());
  }

  cancel(m){
    return this.http.post("http://localhost:8081/bet-app/match/cancel", m, this.getHeaders());
  }

  setMatchResult(r){
    return this.http.post<any>("http://localhost:8081/bet-app/match/set-result", r, this.getHeaders());
  }

  payInMoney(m){
    return this.http.post<any>("http://localhost:8081/bet-app/funds/pay-in", m);
  }

}

export interface user{
  id: number,
  name: string,
  funds: number,
  role: string
}
