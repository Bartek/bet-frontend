import { Component, OnInit, Inject } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { Match } from '../match';
import { MatTableModule, MatTableDataSource } from '@angular/material/table';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Bet } from '../model/Bet';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  dataSource = new MatchDataSource(this._auth);
  endDataSource = new EndMatchDataSource(this._auth);
  displayedColumns = ['startDate', 'firstTeam', 'secondTeam', 't1WinCourse', 'drawCourse', 't2WinCourse', 'bet'];
  endDisplayedColumns = ['startDate', 'firstTeam', 'secondTeam', 't1WinCourse', 'drawCourse', 't2WinCourse', 'cancel', 'setResult'];

  constructor(private _auth: AuthService, public dialog: MatDialog, private _snackBar: MatSnackBar,private _router: Router) {
    this.reloadMatches();
  }

  reloadMatches(){
    
    if(this.userIsAdmin()){
      this.displayedColumns = ['startDate', 'firstTeam', 'secondTeam', 't1WinCourse', 'drawCourse', 't2WinCourse', 'bet', 'cancel'];
    } else{
      this.displayedColumns = ['startDate', 'firstTeam', 'secondTeam', 't1WinCourse', 'drawCourse', 't2WinCourse', 'bet'];
    }

    this.dataSource = new MatchDataSource(this._auth);
    this.endDataSource = new EndMatchDataSource(this._auth);
  }

  ngOnInit(): void {
  }

  bet(match: Match): void {
    if(localStorage.getItem("id") == null){
      return;
    }

    const dialogRef = this.dialog.open(MatchBetDialog, {
      width: '370px',
      data: match
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this._router.navigate(['events']);
    });
  }

  userIsAdmin(): Boolean {
    let role = localStorage.getItem('role');
    if (role === 'admin') {
      console.log("User is admin");
      return true;
    }
    return false;
  }

  createMatch() {
    const dialogRef = this.dialog.open(CreateMatchDialog, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.reloadMatches();
      console.log('The dialog was closed');
    });
  }

  cancel(match: Match){
    let mId = {matchId: match.id}
    this._auth.cancel(mId).subscribe(
      res => {
        this.showMsg("Anulowano mecz");
        this.reloadMatches();
      },
      err => {
        this.showMsg(err.error);
        console.log(err);
      }
    )
  }

  setResult(match: Match){
    const dialogRef = this.dialog.open(SetResultDialog, {
      width: '260px',
      data: match
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.reloadMatches();
    });
  }

  showMsg(msg: string) {
    this._snackBar.open(msg, "", {
      duration: 4000,
    });
  }
}

export class MatchDataSource extends DataSource<any> {
  constructor(private _auth: AuthService) {
    super();
  }
  connect(): Observable<Match[]> {
    return this._auth.getMatches();
  }
  disconnect() { }
}

export class EndMatchDataSource extends DataSource<any> {
  constructor(private _auth: AuthService) {
    super();
  }
  connect(): Observable<Match[]> {

    return this._auth.getEndMatches();
  }
  disconnect() { }
}

@Component({
  selector: 'match-bet-dialog',
  templateUrl: 'match-bet-dialog.html',
})
export class MatchBetDialog {
  match: Match;
  bet: Bet = new Bet();

  constructor(
    private auth: AuthService,
    private _snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<MatchBetDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Match) {

    this.match = data;
    this.bet.matchId = this.match.id;
    this.bet.userId = Number(localStorage.getItem("id"));
    console.log(data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  createBet(): void {

    this.auth.createBet(this.bet).subscribe(
      res => {
        this.dialogRef.close();
        // window.location.reload();
        this.showMsg("Utworzono zakład");
      },
      err => {
        this.showMsg(err.error);
        console.log(err);
      }
    )
  }

  showMsg(msg: string) {
    this._snackBar.open(msg, "", {
      duration: 4000,
    });
  }

}

@Component({
  selector: 'create-match-dialog',
  templateUrl: 'create-match-dialog.html',
})
export class CreateMatchDialog {
  match: CreateMatchModel = new CreateMatchModel();

  constructor(
    private auth: AuthService,
    private _snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<MatchBetDialog>) {
    this.match.startDate = "2020-06-03 18:30";
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  createMatch(): void {
    this.auth.createMatch(this.match).subscribe(
      res => {
        this.dialogRef.close();
        this.showMsg("Utworzono mecz");
      },
      err => {
        this.showMsg(err.error);
        console.log(err);
      }
    )
  }

  showMsg(msg: string) {
    this._snackBar.open(msg, "", {
      duration: 4000,
    });
  }
}

@Component({
  selector: 'set-result-dialog',
  templateUrl: 'set-result-dialog.html',
})
export class SetResultDialog {
  match: Match;
  result: MatchResult = new MatchResult();

  constructor(
    private auth: AuthService,
    private _snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<SetResultDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Match) {

    this.match = data;
    this.result.matchId = data.id;
    console.log(data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  setResult(): void {

    this.auth.setMatchResult(this.result).subscribe(
      res => {
        this.dialogRef.close();
        this.showMsg("Wprowadzono wynik");
      },
      err => {
        this.showMsg(err.error);
        console.log(err);
      }
    )
  }

  showMsg(msg: string) {
    this._snackBar.open(msg, "", {
      duration: 4000,
    });
  }

}

export class MatchResult{
  ftGoal: number;
  matchId: number;
  stGoal: number;
}

export class CreateMatchModel {
  drawCourse: number;
  firstTeam: string;
  secondTeam: string;
  startDate: string;
  t1WinCourse: number;
  t2WinCourse: number;
}